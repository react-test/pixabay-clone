import React, { Component } from 'react'
import Navbar from './navbar/Navbar';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Search from './search/Search';

class App extends Component {

    render() {
        return (<MuiThemeProvider>
            <div>
                <Navbar />
                <Search />
            </div>
        </MuiThemeProvider>);
    }
}

export default App;