import React, { Component } from 'react'
import TextField from 'material-ui/TextField';
import SelecteField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import axios from 'axios';
import ImageResults from '../image-results/ImageResults';


class Search extends Component {

    config = {
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            amount: 5,
            apiUrl: 'https://pixabay.com/api/',
            apiKey: '2659797-31dc84f6d5aa38213f606b922',
            images: []
        };
    }

    onTextChange = event => {
        const url = `${this.state.apiUrl}?key=${this.state.apiKey}&q=${this.state.searchText}&per_page=${this.state.amount}`;
        const val = event.target.value;
        this.setState({ [event.target.name]: val }, () => {
            if (val === '') {
                this.setState({ images: '' })
            } else {
                axios
                    .get(url)
                    .then(res => this.setState({ images: res.data.hits }))
                    .catch(err => console.log(err));
            }
        });

    }
    onSelectChange = (e, index, value) => this.setState({ amount: value });


    render() {
        console.log(this.state.images);
        return (
            <div>
                <TextField
                    name="searchText"
                    floatingLabelText="Search for images"
                    fullWidth={true}
                    value={this.state.searchText}
                    onChange={this.onTextChange} />

                <SelecteField floatingLabelText="Amount"
                    value={this.state.amount}
                    name="amount"
                    onChange={this.onSelectChange}>
                    <MenuItem value={5} primaryText="5" />
                    <MenuItem value={10} primaryText="10" />
                    <MenuItem value={15} primaryText="15" />
                    <MenuItem value={30} primaryText="30" />
                    <MenuItem value={50} primaryText="50" />

                </SelecteField>
                {this.state.images.length > 0 ? (<ImageResults images={this.state.images} />) : null}
            </div>
        );

    }
}

export default Search;
